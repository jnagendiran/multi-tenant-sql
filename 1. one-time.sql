INSERT INTO roles (uuid, name, description, system_role, created_at, updated_at)
VALUES ('7b424ab2-86a0-11e8-a10d-0800273b4cc5', 'root', 'Root Role', 1, NOW(), NOW());


INSERT INTO `tenants` (id, `name`, `email`, `subdomain`, `verified`, `status`, `company`, `street`, `postcode`, `city`, `country`, `account_ready`, `demodata`, `demodata_deleted`, `max_users`, `max_storage`, `max_colas`, `max_pipelines`, `max_dynamic_fields`, `aws_bucket`, `billing_name`, `trial_ends_at`, `plan_start`, `plan_end`, `swap_plan`, `swap_at`, `type`, `premium_upgraded_at`, `verified_at`, `paused_at`, `canceled_at`, `created_at`, `updated_at`)
VALUES
  ('6a5859d4-8f06-11e8-bbc8-0800273b4cc5', 'Maria Kuprian', 'mk@adsolvix.com', 'mk', 1, 'active', NULL, NULL, NULL,
                                           NULL, NULL, 1, 1, 1, -1, 50, -1, -1, -1, 'mk-e07s1ik9fr40zmwq', NULL,
    '2018-02-01 14:00:00', NULL, NULL, NULL, NULL, 'partner', NULL, NULL, NULL, NULL, '2017-09-29 05:03:13',
   '2017-11-23 14:11:06'),
  ('6a585bdb-8f06-11e8-bbc8-0800273b4cc5', 'Ipce', 'fz@fldm.ba', 'fldm', 1, 'active', NULL, NULL, NULL, NULL, NULL, 1, 1, 0, -1, 50, -1, -1, -1, 'fldm-2oibeygb8kv2jmu8', NULL, '2018-02-01 14:00:00', NULL, NULL, NULL, NULL, 'partner', NULL, NULL, NULL, NULL, '2017-09-29 07:30:12', '2017-09-29 07:31:48'),
  ('6a58f5b7-8f06-11e8-bbc8-0800273b4cc5', 'Adam Parusel', 'adam@1salescrm.com', '1sales', 1, 'active', NULL, NULL, NULL, NULL, NULL, 1, 0, 0, -1, 50, -1, -1, -1, '1sales-oxd0htxucywo25ng', NULL, '2018-02-01 14:00:00', NULL, NULL, NULL, NULL, 'partner', NULL, NULL, NULL, NULL, '2017-10-06 07:12:50', '2017-10-06 07:14:50'),
  ('6a58f758-8f06-11e8-bbc8-0800273b4cc5', 'Thorsten Plößer', 'tp@tpvv.de', 'tpvv', 1, 'active', NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, -1, 50, -1, -1, -1, 'tpvv-02czwrpu542lj2hv', NULL, '2018-02-01 14:00:00', NULL, NULL, NULL, NULL, 'partner', NULL, NULL, NULL, NULL, '2017-10-11 15:49:09', '2018-02-01 08:43:03'),
  ('6a58f824-8f06-11e8-bbc8-0800273b4cc5', 'Karsten Eichenhofer', 'service@adsolvix.com', 'legarius', 1, 'active', NULL, NULL, NULL, NULL, NULL, 1, 1, 0, -1, 50, -1, -1, -1, 'legarius-zgputdp5lr6p5r33', NULL, '2018-02-01 14:00:00', NULL, NULL, NULL, NULL, 'partner', NULL, NULL, NULL, NULL, '2017-11-15 11:30:01', '2017-11-15 11:32:27'),
  ('6a58f89e-8f06-11e8-bbc8-0800273b4cc5', 'Karsten', 'karsten@legarius.de', 'karsten', 1, 'active', NULL, NULL, NULL, NULL, NULL, 1, 1, 0, -1, 50, -1, -1, -1, 'karsten-exekvjhb8dgcetz6', NULL, '2018-02-01 14:00:00', NULL, NULL, NULL, NULL, 'partner', NULL, NULL, NULL, NULL, '2017-11-15 18:27:05', '2017-11-15 18:28:23'),
  ('6a58f8ff-8f06-11e8-bbc8-0800273b4cc5', 'Henrik Bredenbals', 'henrik@prechain.de', 'prechain', 1, 'active', NULL, NULL, NULL, NULL, NULL, 1, 0, 0, -1, 50, -1, -1, -1, 'prechain-fdqgwpyd0ej1cza3', NULL, '2018-02-01 14:00:00', NULL, NULL, NULL, NULL, 'partner', NULL, NULL, NULL, NULL, '2017-11-20 08:55:59', '2017-11-20 10:04:24'),
  ('6a58f95b-8f06-11e8-bbc8-0800273b4cc5', 'Sebastian', 'sebastian@foundersfoundation.de', 'foundersfoundation', 1, 'active', NULL, NULL, NULL, NULL, NULL, 1, 1, 0, -1, 50, -1, -1, -1, 'foundersfoundation-gvgva7zoryjczxes', NULL, '2018-02-01 14:00:00', NULL, NULL, NULL, NULL, 'partner', NULL, NULL, NULL, NULL, '2017-11-21 12:20:38', '2017-11-21 12:21:12'),
  ('6a58f9c0-8f06-11e8-bbc8-0800273b4cc5', 'Gerngross', 's.gerngross@prolife-gmbh.de', 'testsg', 1, 'active', NULL, NULL, NULL, NULL, NULL, 1, 1, 0, -1, 50, -1, -1, -1, 'testsg-mrgdgqhbyr8mzkat', NULL, NULL, NULL, NULL, NULL, NULL, 'partner', NULL, NULL, NULL, NULL, '2017-11-22 09:08:31', '2017-11-22 09:16:04'),
  ('6a58fa18-8f06-11e8-bbc8-0800273b4cc5', 'Aylin', 'ak@adsolvix.com', 'aylin', 1, 'active', NULL, NULL, NULL, NULL, NULL, 1, 1, 0, -1, 50, -1, -1, -1, 'aylin-clouupnqzwbxnflk', NULL, '2018-02-01 14:00:00', NULL, NULL, NULL, NULL, 'partner', NULL, NULL, NULL, NULL, '2017-11-22 09:35:16', '2017-11-22 09:35:40'),
  ('6a58fa6f-8f06-11e8-bbc8-0800273b4cc5', 'Alexander Rak', 'alexander.rak@inpunkto24.de', 'inpunkto', 1, 'active', NULL, NULL, NULL, NULL, NULL, 1, 1, 0, -1, 50, -1, -1, -1, 'inpunkto-znhjbg75onlxaihj', NULL, '2018-02-01 14:00:00', NULL, NULL, NULL, NULL, 'partner', NULL, NULL, NULL, NULL, '2017-11-23 10:43:54', '2017-11-23 10:44:28'),
  ('6a58faca-8f06-11e8-bbc8-0800273b4cc5', 'Carsten Janetzky', 'cj@zahnarzt-helden.de', 'zahnarzthelden', 1, 'active', NULL, NULL, NULL, NULL, NULL, 1, 0, 0, -1, 50, -1, -1, -1, 'zahnarzthelden-txn8gufwohbnupew', NULL, '2018-02-01 14:00:00', NULL, NULL, NULL, NULL, 'partner', NULL, NULL, NULL, NULL, '2017-11-23 17:09:47', '2017-11-23 17:10:15'),
  ('6a58fb21-8f06-11e8-bbc8-0800273b4cc5', 'Stefan Waltrup', 'stefanwaltrup@gmail.com', 'waltrup', 1, 'active', NULL, NULL, NULL, NULL, NULL, 1, 0, 0, -1, 50, -1, -1, -1, 'waltrup-aolkueef549utffl', NULL, '2018-02-01 14:00:00', NULL, NULL, NULL, NULL, 'partner', NULL, NULL, NULL, NULL, '2017-11-23 20:46:48', '2017-11-24 08:39:13'),
  ('6a58fd3d-8f06-11e8-bbc8-0800273b4cc5', 'Adsolvix', 'ap@adsolvix.com', 'adsolvix', 1, 'active', NULL, NULL, NULL, NULL, NULL, 1, 0, 0, -1, 50, -1, -1, -1, 'adsolvix.1sales', NULL, '2018-02-01 14:00:00', NULL, NULL, NULL, NULL, 'partner', NULL, NULL, NULL, NULL, '2017-11-23 20:46:48', '2017-11-24 08:39:13'),
  ('6a58fdbd-8f06-11e8-bbc8-0800273b4cc5', 'Sven Möller', 'svenmoeller@me.com', 'svenmoeller', 1, 'active', NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, -1, 50, -1, -1, -1, 'svenmoeller-sq6r5bjgtqfald6c', NULL, '2018-02-01 14:00:00', NULL, NULL, NULL, NULL, 'partner', NULL, NULL, NULL, NULL, '2017-11-26 11:04:21', '2017-11-26 11:04:46'),
  ('6a58fe1a-8f06-11e8-bbc8-0800273b4cc5', 'Robert Keßler', 'robert.kessler@ventanago.com', 'ventanago', 1, 'active', NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, -1, 50, -1, -1, -1, 'ventanago-eafj7q21lpp02em7', NULL, '2018-02-01 14:00:00', NULL, NULL, NULL, NULL, 'partner', NULL, NULL, NULL, NULL, '2017-11-28 06:31:49', '2018-02-01 08:43:04'),
  ('6a58fe73-8f06-11e8-bbc8-0800273b4cc5', 'Birgit Dietze', 'FinanzanlagenDietze@t-online.de', 'dietze', 1, 'active', NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, 1, 1, 100, 1, 3, 'dietze-zvbtn6zq1jeqjfp9', NULL, NULL, NULL, NULL, NULL, NULL, 'free', NULL, NULL, NULL, NULL, '2018-01-09 09:05:44', '2018-01-09 09:06:11'),
  ('6a58fed0-8f06-11e8-bbc8-0800273b4cc5', 'Boris Mracek', 'boris.mracek@archimedes-fm.de', 'afm', 1, 'active', NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, -1, 50, -1, -1, -1, 'afm-rb0q3aj61kv5tpze', NULL, '2018-02-16 10:43:55', NULL, NULL, NULL, NULL, 'partner', NULL, NULL, NULL, NULL, '2018-01-17 10:43:55', '2018-01-17 11:26:20'),
  ('6a58ff28-8f06-11e8-bbc8-0800273b4cc5', 'Test', '1sales@fyii.de', 'test', 1, 'active', NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, 1, 1, 100, 1, 3, 'test-mrcbqfnrz6uzunap', NULL, NULL, NULL, NULL, NULL, NULL, 'free', NULL, NULL, NULL, NULL, '2018-01-24 19:12:32', '2018-01-24 19:12:52'),
  ('6a58ff7e-8f06-11e8-bbc8-0800273b4cc5', 'Ard de Jong', 'info@fhfinanz.de', 'lk', 1, 'active', NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, -1, 50, -1, -1, -1, 'lk-jjd0i7bzfg0wm6ey', NULL, '2018-03-21 12:48:41', NULL, NULL, NULL, NULL, 'partner', NULL, '2018-02-19 13:01:11', NULL, NULL, '2018-02-19 12:48:41', '2018-02-19 13:01:20'),
  ('6a58ffe1-8f06-11e8-bbc8-0800273b4cc5', 'Onat', 'tolgaonat@web.de', 'aspmit', 1, 'active', NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, 1, 1, 100, 1, 3, 'aspmit-kkmrfzq6x0crvtll', NULL, NULL, NULL, NULL, NULL, NULL, 'free', NULL, '2018-03-14 19:40:11', NULL, NULL, '2018-03-14 19:37:57', '2018-03-14 19:40:22'),
  ('6a59003a-8f06-11e8-bbc8-0800273b4cc5', 'Florian Grund', 'florian.grund@inpunkto24.de', 'disbro', 1, 'active', NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, -1, 50, -1, -1, -1, 'disbro-phfxjeboixfvx8dn', NULL, '2018-04-22 11:52:52', NULL, NULL, NULL, NULL, 'partner', NULL, '2018-03-23 12:53:03', '2018-04-22 23:00:03', NULL, '2018-03-23 12:52:52', '2018-04-22 23:00:03'),
  ('6a5900a2-8f06-11e8-bbc8-0800273b4cc5', 'Zimmermann, Thomas', 't.zimmermann@bioeng.de', 'bap', 1, 'active', 'BioArtProducts GmbH', 'Friedrich-Barnewitz-Str. 8', '18119', 'Rostock', 'Deutschland', 1, NULL, 0, -1, 50, -1, -1, -1, 'bap-4lz9kka7yvnappnb', 'Thomas Zimmermann', '2018-05-20 09:24:21', '2018-04-23 08:43:51', '2019-04-22 08:43:51', NULL, NULL, 'premium', NULL, '2018-04-20 09:24:43', NULL, NULL, '2018-04-20 09:24:21', '2018-04-23 08:43:51'),
  ('6a59011b-8f06-11e8-bbc8-0800273b4cc5', 'Arthur Isaak', 'arthur@1sales.io', 'arthurr', 1, 'active', NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, -1, 50, -1, -1, -1, 'arthurr-jcf2mdwgw388swyg', NULL, '2018-05-26 03:55:29', NULL, NULL, NULL, NULL, 'premium', NULL, '2018-04-26 03:55:42', NULL, NULL, '2018-04-26 03:55:29', '2018-04-26 03:55:48'),
  ('6a590179-8f06-11e8-bbc8-0800273b4cc5', 'Heckules Studio', 'studio@heckules.de', 'heckules', 1, 'active', NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, 1, 1, 100, 1, 3, 'heckules-fmswoge2wopq1npy', NULL, NULL, NULL, NULL, NULL, NULL, 'free', NULL, '2018-04-26 11:37:19', NULL, NULL, '2018-04-26 11:33:36', '2018-04-26 11:37:28'),
  ('6a5901d1-8f06-11e8-bbc8-0800273b4cc5', 'JN', 'janny@1sales.io', 'jenna', 1, 'active', '1sales Inc.', 'Wilhelm Straße 1B', '33602', 'Bielefeld', 'DE', 1, NULL, 0, -1, 50, -1, -1, -1, 'jenna-0vqzbri5xrvfdagg', 'JN', '2018-06-02 05:59:28', NULL, NULL, NULL, NULL, 'partner', NULL, '2018-05-03 05:59:39', NULL, NULL, '2018-05-03 05:59:28', '2018-05-03 06:02:58'),
  ('6a590238-8f06-11e8-bbc8-0800273b4cc5', 'Tobias Adrian', 'tobias.adrian@octa-stb.de', 'octa', 1, 'active', NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, -1, 50, -1, -1, -1, 'octa-fxby6me6aa1cdkko', NULL, '2018-06-02 11:24:28', NULL, NULL, NULL, NULL, 'premium', NULL, '2018-05-03 11:46:18', NULL, NULL, '2018-05-03 11:24:28', '2018-05-03 11:46:25'),
  ('6a59029c-8f06-11e8-bbc8-0800273b4cc5', 'Emir Zejnilovic', 'emir@1sales.io', 'emir', 1, 'active', NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, -1, 50, -1, -1, -1, 'emir-wrlb8joycctb75im', NULL, '2018-06-14 08:22:35', NULL, NULL, NULL, NULL, 'premium', NULL, '2018-05-16 04:39:09', NULL, NULL, '2018-05-15 08:22:35', '2018-05-16 04:39:19'),
  ('6a5902f6-8f06-11e8-bbc8-0800273b4cc5', 'Arthur', 'aia@gmx.de', 'arthurarthur', 1, 'active', NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, -1, 50, -1, -1, -1, 'arthurarthur-s9zmwbmhy2vta45f', NULL, '2018-06-15 06:47:34', NULL, NULL, NULL, NULL, 'premium', NULL, '2018-05-16 06:48:16', NULL, NULL, '2018-05-16 06:47:34', '2018-05-16 06:48:23'),
  ('6a590350-8f06-11e8-bbc8-0800273b4cc5', 'Maria Kuprian', 'service@esolvix.com', 'esolvix', 1, 'active', NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, -1, 50, -1, -1, -1, 'esolvix-ihkgjgqmimnrj8e4', NULL, '2018-07-12 14:02:52', NULL, NULL, NULL, NULL, 'premium', NULL, '2018-06-12 14:03:10', NULL, NULL, '2018-06-12 14:02:52', '2018-06-12 14:03:21'),
  ('6a5903ae-8f06-11e8-bbc8-0800273b4cc5', 'Lina', 'lina@1sales.io', 'lina', 1, 'active', NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, -1, 50, -1, -1, -1, 'lina-ibtmasqaif20o1et', NULL, '2018-07-13 06:16:40', NULL, NULL, NULL, NULL, 'premium', NULL, '2018-06-13 06:17:01', NULL, NULL, '2018-06-13 06:16:40', '2018-06-13 06:17:10'),
  ('6a590407-8f06-11e8-bbc8-0800273b4cc5', 'Sanverdi', 'info@sanverdi24.de', 'sanverdi', 1, 'active', NULL, NULL, NULL,
                                           NULL, NULL, 1, NULL, 0, -1, 50, -1, -1, -1, 'sanverdi-gufrmcdd8dynqgdr',
                                                       NULL, '2018-08-13 08:46:15', NULL, NULL, NULL, NULL, 'premium',
                                                             NULL, '2018-07-14 08:46:40', NULL, NULL,
   '2018-07-14 08:46:15', '2018-07-14 08:46:47');


DELIMITER |

# tenant_id = accounts, activities, activity_types, contacts, csv_imports, documents, dynamic_fields, dynamicables, histories, keymaps, leads, lost_reasonables, lost_reasons, notes, opportunities,
# pipelines, roles, settings, stages, users

CREATE TRIGGER accounts_before_insert
BEFORE INSERT ON accounts
FOR EACH ROW
  BEGIN
    SET NEW.uuid = UUID();
  END;
|
CREATE TRIGGER activities_before_insert
BEFORE INSERT ON activities
FOR EACH ROW
  BEGIN
    SET NEW.uuid = UUID();
  END;
|
CREATE TRIGGER activity_types_before_insert
BEFORE INSERT ON activity_types
FOR EACH ROW
  BEGIN
    SET NEW.uuid = UUID();
  END;
|
CREATE TRIGGER contacts_before_insert
BEFORE INSERT ON contacts
FOR EACH ROW
  BEGIN
    SET NEW.uuid = UUID();
  END;
|
CREATE TRIGGER csv_imports_before_insert
BEFORE INSERT ON csv_imports
FOR EACH ROW
  BEGIN
    SET NEW.uuid = UUID();
  END;
|
CREATE TRIGGER documents_before_insert
BEFORE INSERT ON documents
FOR EACH ROW
  BEGIN
    SET NEW.uuid = UUID();
  END;
|
CREATE TRIGGER dynamic_fields_before_insert
BEFORE INSERT ON dynamic_fields
FOR EACH ROW
  BEGIN
    SET NEW.uuid = UUID();
  END;
|
CREATE TRIGGER dynamicables_before_insert
BEFORE INSERT ON dynamicables
FOR EACH ROW
  BEGIN
    SET NEW.uuid = UUID();
  END;
|
CREATE TRIGGER histories_before_insert
BEFORE INSERT ON histories
FOR EACH ROW
  BEGIN
    SET NEW.uuid = UUID();
  END;
|
CREATE TRIGGER keymaps_before_insert
BEFORE INSERT ON keymaps
FOR EACH ROW
  BEGIN
    SET NEW.uuid = UUID();
  END;
|
CREATE TRIGGER leads_before_insert
BEFORE INSERT ON leads
FOR EACH ROW
  BEGIN
    SET NEW.uuid = UUID();
  END;
|
CREATE TRIGGER lost_reasonables_before_insert
BEFORE INSERT ON lost_reasonables
FOR EACH ROW
  BEGIN
    SET NEW.uuid = UUID();
  END;
|
CREATE TRIGGER lost_reasons_before_insert
BEFORE INSERT ON lost_reasons
FOR EACH ROW
  BEGIN
    SET NEW.uuid = UUID();
  END;
|
CREATE TRIGGER notes_before_insert
BEFORE INSERT ON notes
FOR EACH ROW
  BEGIN
    SET NEW.uuid = UUID();
  END;
|
CREATE TRIGGER opportunities_before_insert
BEFORE INSERT ON opportunities
FOR EACH ROW
  BEGIN
    SET NEW.uuid = UUID();
  END;
|
CREATE TRIGGER permissions_before_insert
BEFORE INSERT ON permissions
FOR EACH ROW
  BEGIN
    SET NEW.uuid = UUID();
  END;
|
CREATE TRIGGER pipelines_before_insert
BEFORE INSERT ON pipelines
FOR EACH ROW
  BEGIN
    SET NEW.uuid = UUID();
  END;
|
CREATE TRIGGER roles_before_insert
BEFORE INSERT ON roles
FOR EACH ROW
  BEGIN
    SET NEW.uuid = UUID();
  END;
|
CREATE TRIGGER settings_before_insert
BEFORE INSERT ON settings
FOR EACH ROW
  BEGIN
    SET NEW.uuid = UUID();
  END;
|
CREATE TRIGGER stages_before_insert
BEFORE INSERT ON stages
FOR EACH ROW
  BEGIN
    SET NEW.uuid = UUID();
  END;
|
CREATE TRIGGER users_before_insert
BEFORE INSERT ON users
FOR EACH ROW
  BEGIN
    SET NEW.uuid = UUID();
  END;
|
DELIMITER ;



############ CHECK
SELECT
  count(*),
  uuid
FROM accounts
GROUP BY uuid
HAVING count(*) > 1;

####
DELIMITER $$
DROP PROCEDURE IF EXISTS update_accounts_foreign_uuid $$
CREATE PROCEDURE update_accounts_foreign_uuid(IN param_account_id INT)
  BEGIN
    DECLARE procedure_user_id INT;
    DECLARE procedure_parent_id INT;
    DECLARE procedure_source_lead_id INT;
    DECLARE procedure_csv_import_id INT;
    SET procedure_user_id = (SELECT user_id
                             FROM accounts
                             WHERE id = param_account_id);
    SET procedure_parent_id = (SELECT parent_id
                               FROM accounts
                               WHERE id = param_account_id);
    SET procedure_source_lead_id = (SELECT source_lead_id
                                    FROM accounts
                                    WHERE id = param_account_id);
    SET procedure_csv_import_id = (SELECT csv_import_id
                                   FROM accounts
                                   WHERE id = param_account_id);
    #     DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
    #     BEGIN
    #       ROLLBACK;
    #       RESIGNAL;
    #     END;

    START TRANSACTION;
    UPDATE accounts
    SET uuid_user_id      = (SELECT uuid
                             FROM users
                             WHERE id = procedure_user_id),
      uuid_parent_id      = (SELECT uuid
                             FROM (SELECT *
                                   FROM accounts) AS ac
                             WHERE id = procedure_parent_id),
      uuid_source_lead_id = (SELECT uuid
                             FROM leads
                             WHERE id = procedure_source_lead_id),
      uuid_csv_import_id  = (SELECT uuid
                             FROM csv_imports
                             WHERE id = procedure_csv_import_id)
    WHERE id = param_account_id;
    # TODO: set user_id, parent_id, source_lead_id, csv_import_id to null
    COMMIT;
    SELECT 'success "update_accounts_foreign_uuid"';
  END$$
DELIMITER ;
####
DELIMITER $$
DROP PROCEDURE IF EXISTS update_activities_foreign_uuid $$
CREATE PROCEDURE update_activities_foreign_uuid(IN param_activity_id INT)
  BEGIN
    DECLARE procedure_user_id INT;
    DECLARE procedure_activity_type_id INT;
    DECLARE procedure_activityable_id INT;
    DECLARE procedure_uuid_activityable_id TEXT;
    DECLARE procedure_activityable_type TEXT;
    SET procedure_user_id = (SELECT user_id
                             FROM activities
                             WHERE id = param_activity_id);
    SET procedure_activity_type_id = (SELECT activity_type_id
                                      FROM activities
                                      WHERE id = param_activity_id);
    SET procedure_activityable_id = (SELECT activityable_id
                                     FROM activities
                                     WHERE id = param_activity_id);
    SET procedure_activityable_type = (SELECT activityable_type
                                       FROM activities
                                       WHERE id = param_activity_id);

    IF (procedure_activityable_type IS NOT NULL)
    THEN
      IF (procedure_activityable_type = 'leads')
      THEN
        SET procedure_uuid_activityable_id = (SELECT uuid
                                              FROM leads
                                              WHERE id = procedure_activityable_id);
      ELSEIF (procedure_activityable_type = 'accounts')
        THEN
          SET procedure_uuid_activityable_id = (SELECT uuid
                                                FROM accounts
                                                WHERE id = procedure_activityable_id);
      ELSEIF (procedure_activityable_type = 'contacts')
        THEN
          SET procedure_uuid_activityable_id = (SELECT uuid
                                                FROM contacts
                                                WHERE id = procedure_activityable_id);
      ELSEIF (procedure_activityable_type = 'opportunities')
        THEN
          SET procedure_uuid_activityable_id = (SELECT uuid
                                                FROM opportunities
                                                WHERE id = procedure_activityable_id);
      END IF;
    END IF;

    #     DECLARE EXIT HANDLER FOR SQLEXCEPTION, SQLWARNING
    #     BEGIN
    #       ROLLBACK;
    #       RESIGNAL;
    #     END;

    START TRANSACTION;
    UPDATE activities
    SET uuid_user_id        = (SELECT uuid
                               FROM users
                               WHERE id = procedure_user_id),
      uuid_activity_type_id = (SELECT uuid
                               FROM activity_types
                               WHERE id = procedure_activity_type_id),
      uuid_activityable_id  = procedure_uuid_activityable_id
    WHERE id = param_activity_id;
    # TODO: set user_id, activity_type_id, activityable_id to null
    COMMIT;
    SELECT 'success "update_activities_foreign_uuid"';
  END$$
DELIMITER ;
####
DELIMITER $$
DROP PROCEDURE IF EXISTS update_contacts_foreign_uuid $$
CREATE PROCEDURE update_contacts_foreign_uuid(IN param_contact_id INT)
  BEGIN

    DECLARE procedure_user_id INT;
    DECLARE procedure_account_id INT;
    DECLARE procedure_source_lead_id INT;
    DECLARE procedure_csv_import_id INT;

    SET procedure_user_id = (SELECT user_id
                             FROM contacts
                             WHERE id = param_contact_id);
    SET procedure_account_id = (SELECT account_id
                                FROM contacts
                                WHERE id = param_contact_id);
    SET procedure_source_lead_id = (SELECT source_lead_id
                                    FROM contacts
                                    WHERE id = param_contact_id);
    SET procedure_csv_import_id = (SELECT csv_import_id
                                   FROM contacts
                                   WHERE id = param_contact_id);

    START TRANSACTION;

    UPDATE contacts
    SET uuid_user_id      = (SELECT uuid
                             FROM users
                             WHERE id = procedure_user_id),
      uuid_account_id     = (SELECT uuid
                             FROM accounts
                             WHERE id = procedure_account_id),
      uuid_source_lead_id = (SELECT uuid
                             FROM leads
                             WHERE id = procedure_source_lead_id),
      uuid_csv_import_id  = (SELECT uuid
                             FROM csv_imports
                             WHERE id = procedure_csv_import_id)
    WHERE id = param_contact_id;

    COMMIT;

    SELECT 'success "update_contacts_foreign_uuid"';

  END$$
DELIMITER ;
####
DELIMITER $$
DROP PROCEDURE IF EXISTS update_csv_imports_foreign_uuid $$
CREATE PROCEDURE update_csv_imports_foreign_uuid(IN param_csv_import_id INT)
  BEGIN

    DECLARE procedure_user_id INT;

    SET procedure_user_id = (SELECT user_id
                             FROM csv_imports
                             WHERE id = param_csv_import_id);

    START TRANSACTION;

    UPDATE csv_imports
    SET uuid_user_id = (SELECT uuid
                        FROM users
                        WHERE id = procedure_user_id)
    WHERE id = param_csv_import_id;

    COMMIT;

    SELECT 'success "update_csv_imports_foreign_uuid"';

  END$$
DELIMITER ;
####
DELIMITER $$
DROP PROCEDURE IF EXISTS update_documents_foreign_uuid $$
CREATE PROCEDURE update_documents_foreign_uuid(IN param_document_id INT)
  BEGIN
    DECLARE procedure_user_id INT;
    DECLARE procedure_documentable_id INT;
    DECLARE procedure_uuid_documentable_id TEXT;
    DECLARE procedure_documentable_type TEXT;
    SET procedure_user_id = (SELECT user_id
                             FROM documents
                             WHERE id = param_document_id);
    SET procedure_documentable_id = (SELECT documentable_id
                                     FROM documents
                                     WHERE id = param_document_id);
    SET procedure_documentable_type = (SELECT documentable_type
                                       FROM documents
                                       WHERE id = param_document_id);

    IF (procedure_documentable_type IS NOT NULL)
    THEN
      IF (procedure_documentable_type = 'leads')
      THEN
        SET procedure_uuid_documentable_id = (SELECT uuid
                                              FROM leads
                                              WHERE id = procedure_documentable_id);
      ELSEIF (procedure_documentable_type = 'accounts')
        THEN
          SET procedure_uuid_documentable_id = (SELECT uuid
                                                FROM accounts
                                                WHERE id = procedure_documentable_id);
      ELSEIF (procedure_documentable_type = 'contacts')
        THEN
          SET procedure_uuid_documentable_id = (SELECT uuid
                                                FROM contacts
                                                WHERE id = procedure_documentable_id);
      ELSEIF (procedure_documentable_type = 'opportunities')
        THEN
          SET procedure_uuid_documentable_id = (SELECT uuid
                                                FROM opportunities
                                                WHERE id = procedure_documentable_id);
      END IF;
    END IF;

    START TRANSACTION;

    UPDATE documents
    SET uuid_user_id       = (SELECT uuid
                              FROM users
                              WHERE id = procedure_user_id),
      uuid_documentable_id = procedure_uuid_documentable_id
    WHERE id = param_document_id;

    COMMIT;

    SELECT 'success "update_documents_foreign_uuid"';

  END$$
DELIMITER ;
####
DELIMITER $$
DROP PROCEDURE IF EXISTS update_dynamicables_foreign_uuid $$
CREATE PROCEDURE update_dynamicables_foreign_uuid(IN param_dynamicable_id INT)
  BEGIN

    DECLARE procedure_dynamic_field_id INT;
    DECLARE procedure_dynamicable_id INT;
    DECLARE procedure_uuid_dynamicable_id TEXT;
    DECLARE procedure_dynamicable_type TEXT;

    SET procedure_dynamic_field_id = (SELECT dynamic_field_id
                                      FROM dynamicables
                                      WHERE id = param_dynamicable_id);
    SET procedure_dynamicable_id = (SELECT dynamicable_id
                                    FROM dynamicables
                                    WHERE id = param_dynamicable_id);
    SET procedure_dynamicable_type = (SELECT dynamicable_type
                                      FROM dynamicables
                                      WHERE id = param_dynamicable_id);

    IF (procedure_dynamicable_type IS NOT NULL)
    THEN
      IF (procedure_dynamicable_type = 'leads')
      THEN
        SET procedure_uuid_dynamicable_id = (SELECT uuid
                                             FROM leads
                                             WHERE id = procedure_dynamicable_id);
      ELSEIF (procedure_dynamicable_type = 'accounts')
        THEN
          SET procedure_uuid_dynamicable_id = (SELECT uuid
                                               FROM accounts
                                               WHERE id = procedure_dynamicable_id);
      ELSEIF (procedure_dynamicable_type = 'contacts')
        THEN
          SET procedure_uuid_dynamicable_id = (SELECT uuid
                                               FROM contacts
                                               WHERE id = procedure_dynamicable_id);
      ELSEIF (procedure_dynamicable_type = 'opportunities')
        THEN
          SET procedure_uuid_dynamicable_id = (SELECT uuid
                                               FROM opportunities
                                               WHERE id = procedure_dynamicable_id);
      END IF;
    END IF;

    START TRANSACTION;

    UPDATE dynamicables
    SET uuid_dynamic_field_id = (SELECT uuid
                                 FROM dynamic_fields
                                 WHERE id = procedure_dynamic_field_id),
      uuid_dynamicable_id     = procedure_uuid_dynamicable_id
    WHERE id = param_dynamicable_id;

    COMMIT;

    SELECT 'success "update_dynamicables_foreign_uuid"';

  END$$
DELIMITER ;
####
DELIMITER $$
DROP PROCEDURE IF EXISTS update_histories_foreign_uuid $$
CREATE PROCEDURE update_histories_foreign_uuid(IN param_history_id INT)
  BEGIN

    DECLARE procedure_user_id INT;
    DECLARE procedure_historyable_id INT;
    DECLARE procedure_uuid_historyable_id TEXT;
    DECLARE procedure_historyable_type TEXT;

    SET procedure_user_id = (SELECT user_id
                             FROM histories
                             WHERE id = param_history_id);
    SET procedure_historyable_id = (SELECT historyable_id
                                    FROM histories
                                    WHERE id = param_history_id);
    SET procedure_historyable_type = (SELECT historyable_type
                                      FROM histories
                                      WHERE id = param_history_id);

    IF (procedure_historyable_type IS NOT NULL)
    THEN
      IF (procedure_historyable_type = 'leads')
      THEN
        SET procedure_uuid_historyable_id = (SELECT uuid
                                             FROM leads
                                             WHERE id = procedure_historyable_id);
      ELSEIF (procedure_historyable_type = 'accounts')
        THEN
          SET procedure_uuid_historyable_id = (SELECT uuid
                                               FROM accounts
                                               WHERE id = procedure_historyable_id);
      ELSEIF (procedure_historyable_type = 'contacts')
        THEN
          SET procedure_uuid_historyable_id = (SELECT uuid
                                               FROM contacts
                                               WHERE id = procedure_historyable_id);
      ELSEIF (procedure_historyable_type = 'opportunities')
        THEN
          SET procedure_uuid_historyable_id = (SELECT uuid
                                               FROM opportunities
                                               WHERE id = procedure_historyable_id);
      ELSEIF (procedure_historyable_type = 'users')
        THEN
          SET procedure_uuid_historyable_id = (SELECT uuid
                                               FROM users
                                               WHERE id = procedure_historyable_id);
      END IF;
    END IF;

    START TRANSACTION;

    UPDATE histories
    SET uuid_user_id      = (SELECT uuid
                             FROM users
                             WHERE id = procedure_user_id),
      uuid_historyable_id = procedure_uuid_historyable_id
    WHERE id = param_history_id;

    COMMIT;

    SELECT 'success "update_histories_foreign_uuid"';

  END$$
DELIMITER ;
####
DELIMITER $$
DROP PROCEDURE IF EXISTS update_leads_foreign_uuid $$
CREATE PROCEDURE update_leads_foreign_uuid(IN param_lead_id INT)
  BEGIN

    DECLARE procedure_user_id INT;
    DECLARE procedure_stage_id INT;
    DECLARE procedure_csv_import_id INT;

    SET procedure_user_id = (SELECT user_id
                             FROM leads
                             WHERE id = param_lead_id);
    SET procedure_stage_id = (SELECT stage_id
                              FROM leads
                              WHERE id = param_lead_id);
    SET procedure_csv_import_id = (SELECT csv_import_id
                                   FROM leads
                                   WHERE id = param_lead_id);

    START TRANSACTION;

    UPDATE leads
    SET uuid_user_id     = (SELECT uuid
                            FROM users
                            WHERE id = procedure_user_id),
      uuid_stage_id      = (SELECT uuid
                            FROM stages
                            WHERE id = procedure_stage_id),
      uuid_csv_import_id = (SELECT uuid
                            FROM csv_imports
                            WHERE id = procedure_csv_import_id)
    WHERE id = param_lead_id;

    COMMIT;

    SELECT 'success "update_leads_foreign_uuid"';

  END$$
DELIMITER ;
####
DELIMITER $$
DROP PROCEDURE IF EXISTS update_lost_reasonables_foreign_uuid $$
CREATE PROCEDURE update_lost_reasonables_foreign_uuid(IN param_lost_reasonable_id INT)
  BEGIN

    DECLARE procedure_lost_reason_id INT;
    DECLARE procedure_lost_reasonable_id INT;
    DECLARE procedure_uuid_lost_reasonable_id TEXT;
    DECLARE procedure_lost_reasonable_type TEXT;

    SET procedure_lost_reason_id = (SELECT lost_reason_id
                                    FROM lost_reasonables
                                    WHERE id = param_lost_reasonable_id);
    SET procedure_lost_reasonable_id = (SELECT lost_reasonable_id
                                        FROM lost_reasonables
                                        WHERE id = param_lost_reasonable_id);
    SET procedure_lost_reasonable_type = (SELECT lost_reasonable_type
                                          FROM lost_reasonables
                                          WHERE id = param_lost_reasonable_id);

    IF (procedure_lost_reasonable_type IS NOT NULL)
    THEN
      IF (procedure_lost_reasonable_type = 'leads')
      THEN
        SET procedure_uuid_lost_reasonable_id = (SELECT uuid
                                                 FROM leads
                                                 WHERE id = procedure_lost_reasonable_id);
      ELSEIF (procedure_lost_reasonable_type = 'opportunities')
        THEN
          SET procedure_uuid_lost_reasonable_id = (SELECT uuid
                                                   FROM opportunities
                                                   WHERE id = procedure_lost_reasonable_id);
      END IF;
    END IF;

    START TRANSACTION;

    UPDATE lost_reasonables
    SET uuid_lost_reason_id   = (SELECT uuid
                                 FROM lost_reasons
                                 WHERE id = procedure_lost_reason_id),
      uuid_lost_reasonable_id = procedure_uuid_lost_reasonable_id
    WHERE id = param_lost_reasonable_id;

    COMMIT;

    SELECT 'success "update_lost_reasonables_foreign_uuid"';

  END$$
DELIMITER ;
####
DELIMITER $$
DROP PROCEDURE IF EXISTS update_notes_foreign_uuid $$
CREATE PROCEDURE update_notes_foreign_uuid(IN param_note_id INT)
  BEGIN

    DECLARE procedure_user_id INT;
    DECLARE procedure_noteable_id INT;
    DECLARE procedure_uuid_noteable_id TEXT;
    DECLARE procedure_noteable_type TEXT;

    SET procedure_user_id = (SELECT user_id
                             FROM notes
                             WHERE id = param_note_id);
    SET procedure_noteable_id = (SELECT noteable_id
                                 FROM notes
                                 WHERE id = param_note_id);
    SET procedure_noteable_type = (SELECT noteable_type
                                   FROM notes
                                   WHERE id = param_note_id);

    IF (procedure_noteable_type IS NOT NULL)
    THEN
      IF (procedure_noteable_type = 'leads')
      THEN
        SET procedure_uuid_noteable_id = (SELECT uuid
                                          FROM leads
                                          WHERE id = procedure_noteable_id);
      ELSEIF (procedure_noteable_type = 'accounts')
        THEN
          SET procedure_uuid_noteable_id = (SELECT uuid
                                            FROM accounts
                                            WHERE id = procedure_noteable_id);
      ELSEIF (procedure_noteable_type = 'contacts')
        THEN
          SET procedure_uuid_noteable_id = (SELECT uuid
                                            FROM contacts
                                            WHERE id = procedure_noteable_id);
      ELSEIF (procedure_noteable_type = 'opportunities')
        THEN
          SET procedure_uuid_noteable_id = (SELECT uuid
                                            FROM opportunities
                                            WHERE id = procedure_noteable_id);
      END IF;
    END IF;

    START TRANSACTION;

    UPDATE notes
    SET uuid_user_id   = (SELECT uuid
                          FROM users
                          WHERE id = procedure_user_id),
      uuid_noteable_id = procedure_uuid_noteable_id
    WHERE id = param_note_id;

    COMMIT;

    SELECT 'success "update_notes_foreign_uuid"';

  END$$
DELIMITER ;
####
DELIMITER $$
DROP PROCEDURE IF EXISTS update_opportunities_foreign_uuid $$
CREATE PROCEDURE update_opportunities_foreign_uuid(IN param_opportunity_id INT)
  BEGIN

    DECLARE procedure_user_id INT;
    DECLARE procedure_account_id INT;
    DECLARE procedure_contact_id INT;
    DECLARE procedure_stage_id INT;

    SET procedure_user_id = (SELECT user_id
                             FROM opportunities
                             WHERE id = param_opportunity_id);
    SET procedure_account_id = (SELECT account_id
                                FROM opportunities
                                WHERE id = param_opportunity_id);
    SET procedure_contact_id = (SELECT contact_id
                                FROM opportunities
                                WHERE id = param_opportunity_id);
    SET procedure_stage_id = (SELECT stage_id
                              FROM opportunities
                              WHERE id = param_opportunity_id);

    START TRANSACTION;

    UPDATE opportunities
    SET uuid_user_id  = (SELECT uuid
                         FROM users
                         WHERE id = procedure_user_id),
      uuid_account_id = (SELECT uuid
                         FROM accounts
                         WHERE id = procedure_account_id),
      uuid_contact_id = (SELECT uuid
                         FROM contacts
                         WHERE id = procedure_contact_id),
      uuid_stage_id   = (SELECT uuid
                         FROM stages
                         WHERE id = procedure_stage_id)
    WHERE id = param_opportunity_id;

    COMMIT;

    SELECT 'success "update_opportunities_foreign_uuid"';

  END$$
DELIMITER ;
####
DELIMITER $$
DROP PROCEDURE IF EXISTS update_permission_role_foreign_uuid $$
CREATE PROCEDURE update_permission_role_foreign_uuid(IN param_role_id INT, param_permission_id INT)
  BEGIN

    DECLARE procedure_instance_root_role_uuid TEXT;
    SET procedure_instance_root_role_uuid = (SELECT uuid
                                             FROM roles
                                             WHERE uuid != '7b424ab2-86a0-11e8-a10d-0800273b4cc5' AND name = 'root');

    START TRANSACTION;

    UPDATE permission_role
    SET uuid_role_id     = (SELECT uuid
                            FROM roles
                            WHERE id = param_role_id),
      uuid_permission_id = (SELECT uuid
                            FROM permissions
                            WHERE id = param_permission_id)
    WHERE role_id = param_role_id AND permission_id = param_permission_id;

    IF NOT EXISTS(SELECT *
                  FROM permission_role
                  WHERE uuid_role_id = '7b424ab2-86a0-11e8-a10d-0800273b4cc5' AND uuid_permission_id = (SELECT uuid
                                                                                                        FROM permissions
                                                                                                        WHERE id =
                                                                                                              param_permission_id))
    THEN
      UPDATE permission_role
      SET uuid_role_id = '7b424ab2-86a0-11e8-a10d-0800273b4cc5'
      WHERE uuid_role_id = procedure_instance_root_role_uuid AND uuid_permission_id = (SELECT uuid
                                                                                       FROM permissions
                                                                                       WHERE id = param_permission_id);
    ELSE
      DELETE
      FROM permission_role
      WHERE uuid_role_id = procedure_instance_root_role_uuid AND uuid_permission_id = (SELECT uuid
                                                                                       FROM permissions
                                                                                       WHERE id = param_permission_id);

    END IF;

    UPDATE permission_role
    SET role_id = NULL, permission_id = NULL
    WHERE role_id = param_role_id AND permission_id = param_permission_id;

    COMMIT;

    SELECT 'success "update_permission_role_foreign_uuid"';

  END$$
DELIMITER ;
####
DELIMITER $$
DROP PROCEDURE IF EXISTS update_role_user_foreign_uuid $$
CREATE PROCEDURE update_role_user_foreign_uuid(IN param_role_id INT, param_user_id INT)
  BEGIN

    DECLARE procedure_instance_root_role_uuid TEXT;
    SET procedure_instance_root_role_uuid = (SELECT uuid
                                             FROM roles
                                             WHERE uuid != '7b424ab2-86a0-11e8-a10d-0800273b4cc5' AND name = 'root');

    START TRANSACTION;

    UPDATE role_user
    SET uuid_role_id = (SELECT uuid
                        FROM roles
                        WHERE id = param_role_id),
      uuid_user_id   = (SELECT uuid
                        FROM users
                        WHERE id = param_user_id)
    WHERE role_id = param_role_id AND user_id = param_user_id;


    IF NOT EXISTS(SELECT *
                  FROM role_user
                  WHERE uuid_role_id = '7b424ab2-86a0-11e8-a10d-0800273b4cc5' AND uuid_user_id = (SELECT uuid
                                                                                                  FROM users
                                                                                                  WHERE
                                                                                                    id = param_user_id))
    THEN
      UPDATE role_user
      SET uuid_role_id = '7b424ab2-86a0-11e8-a10d-0800273b4cc5'
      WHERE uuid_role_id = procedure_instance_root_role_uuid AND uuid_user_id = (SELECT uuid
                                                                                 FROM users
                                                                                 WHERE id = param_user_id);
    ELSE
      DELETE
      FROM role_user
      WHERE uuid_role_id = procedure_instance_root_role_uuid AND uuid_user_id = (SELECT uuid
                                                                                 FROM users
                                                                                 WHERE id = param_user_id);

    END IF;

    UPDATE role_user
    SET role_id = NULL, user_id = NULL
    WHERE role_id = param_role_id AND user_id = param_user_id;

    COMMIT;

    SELECT 'success "update_role_user_foreign_uuid"';

  END$$
DELIMITER ;
####
DELIMITER $$
DROP PROCEDURE IF EXISTS update_roles_foreign_uuid $$
CREATE PROCEDURE update_roles_foreign_uuid()
  BEGIN

    START TRANSACTION;

    DELETE FROM roles
    WHERE name = 'root' AND uuid != '7b424ab2-86a0-11e8-a10d-0800273b4cc5';

    COMMIT;

    SELECT 'success "update_roles_foreign_uuid"';

  END$$
DELIMITER ;
####
DELIMITER $$
DROP PROCEDURE IF EXISTS update_settings_foreign_uuid $$
CREATE PROCEDURE update_settings_foreign_uuid(IN param_setting_id INT)
  BEGIN

    DECLARE procedure_user_id INT;

    SET procedure_user_id = (SELECT user_id
                             FROM settings
                             WHERE id = param_setting_id);

    START TRANSACTION;

    UPDATE settings
    SET uuid_user_id = (SELECT uuid
                        FROM users
                        WHERE id = procedure_user_id)
    WHERE id = param_setting_id;

    COMMIT;

    SELECT 'success "update_settings_foreign_uuid"';

  END$$
DELIMITER ;
####
DELIMITER $$
DROP PROCEDURE IF EXISTS update_stages_foreign_uuid $$
CREATE PROCEDURE update_stages_foreign_uuid(IN param_stage_id INT)
  BEGIN

    DECLARE procedure_pipeline_id INT;

    SET procedure_pipeline_id = (SELECT pipeline_id
                                 FROM stages
                                 WHERE id = param_stage_id);

    START TRANSACTION;

    UPDATE stages
    SET uuid_pipeline_id = (SELECT uuid
                            FROM pipelines
                            WHERE id = procedure_pipeline_id)
    WHERE id = param_stage_id;

    COMMIT;

    SELECT 'success "update_stages_foreign_uuid"';

  END$$
DELIMITER ;
####
DELIMITER $$
DROP PROCEDURE IF EXISTS update_users_foreign_uuid $$
CREATE PROCEDURE update_users_foreign_uuid(IN param_user_id INT)
  BEGIN

    DECLARE procedure_parent_id INT;

    SET procedure_parent_id = (SELECT parent_id
                               FROM users
                               WHERE id = param_user_id);

    START TRANSACTION;

    UPDATE users
    SET uuid_parent_id = (SELECT uuid
                          FROM (SELECT *
                                FROM users) AS u
                          WHERE u.id = procedure_parent_id)
    WHERE id = param_user_id;

    COMMIT;

    SELECT 'success "update_users_foreign_uuid"';

  END$$
DELIMITER ;
####
DELIMITER $$
DROP PROCEDURE IF EXISTS update_all_imported_rows_id_into_null $$
CREATE PROCEDURE update_all_imported_rows_id_into_null()
  BEGIN

    UPDATE accounts
    SET id = NULL
    WHERE id IS NOT NULL;

    UPDATE activities
    SET id = NULL
    WHERE id IS NOT NULL;

    UPDATE activity_types
    SET id = NULL
    WHERE id IS NOT NULL;

    UPDATE contacts
    SET id = NULL
    WHERE id IS NOT NULL;

    UPDATE csv_imports
    SET id = NULL
    WHERE id IS NOT NULL;

    UPDATE documents
    SET id = NULL
    WHERE id IS NOT NULL;

    UPDATE dynamic_fields
    SET id = NULL
    WHERE id IS NOT NULL;

    UPDATE dynamicables
    SET id = NULL
    WHERE id IS NOT NULL;

    UPDATE histories
    SET id = NULL
    WHERE id IS NOT NULL;

    UPDATE keymaps
    SET id = NULL
    WHERE id IS NOT NULL;

    UPDATE leads
    SET id = NULL
    WHERE id IS NOT NULL;

    UPDATE lost_reasonables
    SET id = NULL
    WHERE id IS NOT NULL;

    UPDATE lost_reasons
    SET id = NULL
    WHERE id IS NOT NULL;

    UPDATE notes
    SET id = NULL
    WHERE id IS NOT NULL;

    UPDATE opportunities
    SET id = NULL
    WHERE id IS NOT NULL;

    UPDATE pipelines
    SET id = NULL
    WHERE id IS NOT NULL;

    UPDATE roles
    SET id = NULL
    WHERE id IS NOT NULL;

    UPDATE settings
    SET id = NULL
    WHERE id IS NOT NULL;

    UPDATE stages
    SET id = NULL
    WHERE id IS NOT NULL;

    UPDATE users
    SET id = NULL
    WHERE id IS NOT NULL;


  END;
DELIMITER ;





########
DELIMITER $$
DROP PROCEDURE IF EXISTS loop_through_all_account_rows $$
CREATE PROCEDURE loop_through_all_account_rows()
  BEGIN
    DECLARE cursor_ID INT;
    DECLARE done INT DEFAULT FALSE;
    DECLARE cursor_i CURSOR FOR SELECT id
                                FROM accounts
                                WHERE id IS NOT NULL;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    OPEN cursor_i;
    read_loop: LOOP
      FETCH cursor_i
      INTO cursor_ID;
      IF done
      THEN
        LEAVE read_loop;
      END IF;
      CALL update_accounts_foreign_uuid(cursor_ID);
    END LOOP;
    CLOSE cursor_i;
  END;
DELIMITER ;
#########
DELIMITER $$
DROP PROCEDURE IF EXISTS loop_through_all_activity_rows $$
CREATE PROCEDURE loop_through_all_activity_rows()
  BEGIN
    DECLARE cursor_ID INT;
    DECLARE done INT DEFAULT FALSE;
    DECLARE cursor_i CURSOR FOR SELECT id
                                FROM activities
                                WHERE id IS NOT NULL;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    OPEN cursor_i;
    read_loop: LOOP
      FETCH cursor_i
      INTO cursor_ID;
      IF done
      THEN
        LEAVE read_loop;
      END IF;
      CALL update_activities_foreign_uuid(cursor_ID);
    END LOOP;
    CLOSE cursor_i;
  END;
DELIMITER ;
#########
DELIMITER $$
DROP PROCEDURE IF EXISTS loop_through_all_contact_rows $$
CREATE PROCEDURE loop_through_all_contact_rows()
  BEGIN
    DECLARE cursor_ID INT;
    DECLARE done INT DEFAULT FALSE;
    DECLARE cursor_i CURSOR FOR SELECT id
                                FROM contacts
                                WHERE id IS NOT NULL;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    OPEN cursor_i;
    read_loop: LOOP
      FETCH cursor_i
      INTO cursor_ID;
      IF done
      THEN
        LEAVE read_loop;
      END IF;
      CALL update_contacts_foreign_uuid(cursor_ID);
    END LOOP;
    CLOSE cursor_i;
  END;
DELIMITER ;
#########
DELIMITER $$
DROP PROCEDURE IF EXISTS loop_through_all_csv_import_rows $$
CREATE PROCEDURE loop_through_all_csv_import_rows()
  BEGIN
    DECLARE cursor_ID INT;
    DECLARE done INT DEFAULT FALSE;
    DECLARE cursor_i CURSOR FOR SELECT id
                                FROM csv_imports
                                WHERE id IS NOT NULL;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    OPEN cursor_i;
    read_loop: LOOP
      FETCH cursor_i
      INTO cursor_ID;
      IF done
      THEN
        LEAVE read_loop;
      END IF;
      CALL update_csv_imports_foreign_uuid(cursor_ID);
    END LOOP;
    CLOSE cursor_i;
  END;
DELIMITER ;
#########
DELIMITER $$
DROP PROCEDURE IF EXISTS loop_through_all_document_rows $$
CREATE PROCEDURE loop_through_all_document_rows()
  BEGIN
    DECLARE cursor_ID INT;
    DECLARE done INT DEFAULT FALSE;
    DECLARE cursor_i CURSOR FOR SELECT id
                                FROM documents
                                WHERE id IS NOT NULL;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    OPEN cursor_i;
    read_loop: LOOP
      FETCH cursor_i
      INTO cursor_ID;
      IF done
      THEN
        LEAVE read_loop;
      END IF;
      CALL update_documents_foreign_uuid(cursor_ID);
    END LOOP;
    CLOSE cursor_i;
  END;
DELIMITER ;
#########
DELIMITER $$
DROP PROCEDURE IF EXISTS loop_through_all_dynamicable_rows $$
CREATE PROCEDURE loop_through_all_dynamicable_rows()
  BEGIN
    DECLARE cursor_ID INT;
    DECLARE done INT DEFAULT FALSE;
    DECLARE cursor_i CURSOR FOR SELECT id
                                FROM dynamicables
                                WHERE id IS NOT NULL;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    OPEN cursor_i;
    read_loop: LOOP
      FETCH cursor_i
      INTO cursor_ID;
      IF done
      THEN
        LEAVE read_loop;
      END IF;
      CALL update_dynamicables_foreign_uuid(cursor_ID);
    END LOOP;
    CLOSE cursor_i;
  END;
DELIMITER ;
#########
DELIMITER $$
DROP PROCEDURE IF EXISTS loop_through_all_history_rows $$
CREATE PROCEDURE loop_through_all_history_rows()
  BEGIN
    DECLARE cursor_ID INT;
    DECLARE done INT DEFAULT FALSE;
    DECLARE cursor_i CURSOR FOR SELECT id
                                FROM histories
                                WHERE id IS NOT NULL;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    OPEN cursor_i;
    read_loop: LOOP
      FETCH cursor_i
      INTO cursor_ID;
      IF done
      THEN
        LEAVE read_loop;
      END IF;
      CALL update_histories_foreign_uuid(cursor_ID);
    END LOOP;
    CLOSE cursor_i;
  END;
DELIMITER ;
#########
DELIMITER $$
DROP PROCEDURE IF EXISTS loop_through_all_lead_rows $$
CREATE PROCEDURE loop_through_all_lead_rows()
  BEGIN
    DECLARE cursor_ID INT;
    DECLARE done INT DEFAULT FALSE;
    DECLARE cursor_i CURSOR FOR SELECT id
                                FROM leads
                                WHERE id IS NOT NULL;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    OPEN cursor_i;
    read_loop: LOOP
      FETCH cursor_i
      INTO cursor_ID;
      IF done
      THEN
        LEAVE read_loop;
      END IF;
      CALL update_leads_foreign_uuid(cursor_ID);
    END LOOP;
    CLOSE cursor_i;
  END;
DELIMITER ;
#########
DELIMITER $$
DROP PROCEDURE IF EXISTS loop_through_all_lost_reasonable_rows $$
CREATE PROCEDURE loop_through_all_lost_reasonable_rows()
  BEGIN
    DECLARE cursor_ID INT;
    DECLARE done INT DEFAULT FALSE;
    DECLARE cursor_i CURSOR FOR SELECT id
                                FROM lost_reasonables
                                WHERE id IS NOT NULL;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    OPEN cursor_i;
    read_loop: LOOP
      FETCH cursor_i
      INTO cursor_ID;
      IF done
      THEN
        LEAVE read_loop;
      END IF;
      CALL update_lost_reasonables_foreign_uuid(cursor_ID);
    END LOOP;
    CLOSE cursor_i;
  END;
DELIMITER ;
#########
DELIMITER $$
DROP PROCEDURE IF EXISTS loop_through_all_note_rows $$
CREATE PROCEDURE loop_through_all_note_rows()
  BEGIN
    DECLARE cursor_ID INT;
    DECLARE done INT DEFAULT FALSE;
    DECLARE cursor_i CURSOR FOR SELECT id
                                FROM notes
                                WHERE id IS NOT NULL;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    OPEN cursor_i;
    read_loop: LOOP
      FETCH cursor_i
      INTO cursor_ID;
      IF done
      THEN
        LEAVE read_loop;
      END IF;
      CALL update_notes_foreign_uuid(cursor_ID);
    END LOOP;
    CLOSE cursor_i;
  END;
DELIMITER ;
#########
DELIMITER $$
DROP PROCEDURE IF EXISTS loop_through_all_opportunity_rows $$
CREATE PROCEDURE loop_through_all_opportunity_rows()
  BEGIN
    DECLARE cursor_ID INT;
    DECLARE done INT DEFAULT FALSE;
    DECLARE cursor_i CURSOR FOR SELECT id
                                FROM opportunities
                                WHERE id IS NOT NULL;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    OPEN cursor_i;
    read_loop: LOOP
      FETCH cursor_i
      INTO cursor_ID;
      IF done
      THEN
        LEAVE read_loop;
      END IF;
      CALL update_opportunities_foreign_uuid(cursor_ID);
    END LOOP;
    CLOSE cursor_i;
  END;
DELIMITER ;
#########
DELIMITER $$
DROP PROCEDURE IF EXISTS loop_through_all_permission_role_rows $$
CREATE PROCEDURE loop_through_all_permission_role_rows()
  BEGIN
    DECLARE local_role_ID INT;
    DECLARE local_permission_ID INT;
    DECLARE done INT DEFAULT FALSE;
    DECLARE cursor_i CURSOR FOR SELECT
                                  role_id,
                                  permission_id
                                FROM permission_role
                                WHERE role_id IS NOT NULL AND permission_id IS NOT NULL;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    OPEN cursor_i;
    read_loop: LOOP
      FETCH cursor_i
      INTO local_role_ID, local_permission_ID;
      IF done
      THEN
        LEAVE read_loop;
      END IF;
      CALL update_permission_role_foreign_uuid(local_role_ID, local_permission_ID);
    END LOOP;
    CLOSE cursor_i;
  END;
DELIMITER ;
#########
DELIMITER $$
DROP PROCEDURE IF EXISTS loop_through_all_role_user_rows $$
CREATE PROCEDURE loop_through_all_role_user_rows()
  BEGIN
    DECLARE local_role_ID INT;
    DECLARE local_user_ID INT;
    DECLARE done INT DEFAULT FALSE;
    DECLARE cursor_i CURSOR FOR SELECT
                                  role_id,
                                  user_id
                                FROM role_user
                                WHERE role_id IS NOT NULL AND user_id IS NOT NULL;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    OPEN cursor_i;
    read_loop: LOOP
      FETCH cursor_i
      INTO local_role_ID, local_user_ID;
      IF done
      THEN
        LEAVE read_loop;
      END IF;
      CALL update_role_user_foreign_uuid(local_role_ID, local_user_ID);
    END LOOP;
    CLOSE cursor_i;
  END;
DELIMITER ;
#########
DELIMITER $$
DROP PROCEDURE IF EXISTS loop_through_all_setting_rows $$
CREATE PROCEDURE loop_through_all_setting_rows()
  BEGIN
    DECLARE cursor_ID INT;
    DECLARE done INT DEFAULT FALSE;
    DECLARE cursor_i CURSOR FOR SELECT id
                                FROM settings
                                WHERE id IS NOT NULL;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    OPEN cursor_i;
    read_loop: LOOP
      FETCH cursor_i
      INTO cursor_ID;
      IF done
      THEN
        LEAVE read_loop;
      END IF;
      CALL update_settings_foreign_uuid(cursor_ID);
    END LOOP;
    CLOSE cursor_i;
  END;
DELIMITER ;
#########
DELIMITER $$
DROP PROCEDURE IF EXISTS loop_through_all_stage_rows $$
CREATE PROCEDURE loop_through_all_stage_rows()
  BEGIN
    DECLARE cursor_ID INT;
    DECLARE done INT DEFAULT FALSE;
    DECLARE cursor_i CURSOR FOR SELECT id
                                FROM stages
                                WHERE id IS NOT NULL;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    OPEN cursor_i;
    read_loop: LOOP
      FETCH cursor_i
      INTO cursor_ID;
      IF done
      THEN
        LEAVE read_loop;
      END IF;
      CALL update_stages_foreign_uuid(cursor_ID);
    END LOOP;
    CLOSE cursor_i;
  END;
DELIMITER ;
#########
DELIMITER $$
DROP PROCEDURE IF EXISTS loop_through_all_user_rows $$
CREATE PROCEDURE loop_through_all_user_rows()
  BEGIN
    DECLARE cursor_ID INT;
    DECLARE done INT DEFAULT FALSE;
    DECLARE cursor_i CURSOR FOR SELECT id
                                FROM users
                                WHERE id IS NOT NULL;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    OPEN cursor_i;
    read_loop: LOOP
      FETCH cursor_i
      INTO cursor_ID;
      IF done
      THEN
        LEAVE read_loop;
      END IF;
      CALL update_users_foreign_uuid(cursor_ID);
    END LOOP;
    CLOSE cursor_i;
  END;
DELIMITER ;
#########
DELIMITER $$
DROP PROCEDURE IF EXISTS set_tenant_id_for_all_rows $$
CREATE PROCEDURE set_tenant_id_for_all_rows(IN param_tenant_id CHAR(36))
  BEGIN
    UPDATE notes
    SET tenant_id = param_tenant_id
    WHERE tenant_id IS NULL;

    UPDATE histories
    SET tenant_id = param_tenant_id
    WHERE tenant_id IS NULL;

    UPDATE leads
    SET tenant_id = param_tenant_id
    WHERE tenant_id IS NULL;

    UPDATE documents
    SET tenant_id = param_tenant_id
    WHERE tenant_id IS NULL;

    UPDATE opportunities
    SET tenant_id = param_tenant_id
    WHERE tenant_id IS NULL;

    UPDATE csv_imports
    SET tenant_id = param_tenant_id
    WHERE tenant_id IS NULL;

    UPDATE pipelines
    SET tenant_id = param_tenant_id
    WHERE tenant_id IS NULL;

    UPDATE keymaps
    SET tenant_id = param_tenant_id
    WHERE tenant_id IS NULL;

    UPDATE activities
    SET tenant_id = param_tenant_id
    WHERE tenant_id IS NULL;

    UPDATE dynamic_fields
    SET tenant_id = param_tenant_id
    WHERE tenant_id IS NULL;

    UPDATE dynamicables
    SET tenant_id = param_tenant_id
    WHERE tenant_id IS NULL;

    UPDATE lost_reasonables
    SET tenant_id = param_tenant_id
    WHERE tenant_id IS NULL;

    UPDATE accounts
    SET tenant_id = param_tenant_id
    WHERE tenant_id IS NULL;

    UPDATE lost_reasons
    SET tenant_id = param_tenant_id
    WHERE tenant_id IS NULL;

    UPDATE contacts
    SET tenant_id = param_tenant_id
    WHERE tenant_id IS NULL;

    UPDATE settings
    SET tenant_id = param_tenant_id
    WHERE tenant_id IS NULL;

    UPDATE activity_types
    SET tenant_id = param_tenant_id
    WHERE tenant_id IS NULL;

    UPDATE stages
    SET tenant_id = param_tenant_id
    WHERE tenant_id IS NULL;

    UPDATE users
    SET tenant_id = param_tenant_id
    WHERE tenant_id IS NULL;

    UPDATE roles
    SET tenant_id = param_tenant_id
    WHERE tenant_id IS NULL AND system_role = FALSE;
  END;
DELIMITER ;
#########


#########
# CALL loop_through_all_account_rows();
# CALL loop_through_all_activity_rows();
# CALL loop_through_all_contact_rows();
# CALL loop_through_all_csv_import_rows();
# CALL loop_through_all_document_rows();
# CALL loop_through_all_dynamicable_rows();
# CALL loop_through_all_history_rows();
# CALL loop_through_all_lead_rows();
# CALL loop_through_all_lost_reasonable_rows();
# CALL loop_through_all_note_rows();
# CALL loop_through_all_opportunity_rows();
# CALL loop_through_all_permission_role_rows();
# CALL loop_through_all_role_user_rows();
# CALL loop_through_all_setting_rows();
# CALL loop_through_all_stage_rows();
# CALL loop_through_all_user_rows();
# CALL update_roles_foreign_uuid();
# CALL update_all_imported_rows_id_into_null();
# CALL set_tenant_id_for_all_rows('6a58fd3d-8f06-11e8-bbc8-0800273b4cc5');
#########

# SELECT tenant_id, count(*)
# from users
# GROUP BY tenant_id;

DELETE
FROM migrations
WHERE migration = '2018_08_06_140749_modify_uuid_relations';