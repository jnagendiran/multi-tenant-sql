# Readme:

```bash
1. Step: Open class 'ModifyUuidRelations' comment out line 18 and 21
2. Step: Run 'php artisan migrate:fresh'
3. Step: Run '1. one-time.sql'

4. Step: Open SQL PRO and PRESS CMD+SHIFT+I and import 'db-4.sql' (Ignore Alert and click 'Ignore All Errors')
5. Step: OPEN '2. after-each-tenant-import.sql', replace '6a58fd3d-8f06-11e8-bbc8-0800273b4cc5' with actual tenant_id in the last procedure call.
6. Step: RUN '2. after-each-tenant-import.sql'
7. Step: RUN 'test.sql'

8. Step: Repeat Step 5-7 for each tenant-import

9. Step: Open class 'ModifyUuidRelations' and undo comment in line 18 and 21
10. Step: Run 'php artisan migrate'
11. Step: Run '3. very-last-script.sql'
```