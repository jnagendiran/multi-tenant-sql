DELIMITER $$
DROP PROCEDURE IF EXISTS do_test $$

CREATE PROCEDURE do_test()
  BEGIN
    DECLARE specialty CONDITION FOR SQLSTATE '45000';




    -- ACCOUNTS -----------------------------------------------------------------------------
    -- source lead
    IF EXISTS(SELECT * FROM accounts
      JOIN leads ON leads.id = accounts.source_lead_id
    WHERE accounts.id IS NOT NULL AND (leads.tenant_id <> accounts.tenant_id OR leads.uuid <> accounts.uuid_source_lead_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: accounts.source_lead_id';
    END IF;

    -- csv import
    IF EXISTS(SELECT * FROM accounts
      JOIN csv_imports ON csv_imports.id = accounts.csv_import_id
    WHERE accounts.id IS NOT NULL AND (csv_imports.tenant_id <> accounts.tenant_id OR csv_imports.uuid <> accounts.uuid_csv_import_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: accounts.csv_import_id';
    END IF;

    -- user
    IF EXISTS(SELECT * FROM accounts
      JOIN users ON users.id = accounts.user_id
    WHERE accounts.id IS NOT NULL AND (users.tenant_id <> accounts.tenant_id OR users.uuid <> accounts.uuid_user_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: accounts.user_id';
    END IF;




    -- ACTIVITIES ---------------------------------------------------------------------------
    -- user
    IF EXISTS(SELECT * FROM activities
      JOIN users ON users.id = activities.user_id
    WHERE activities.id IS NOT NULL AND (users.tenant_id <> activities.tenant_id OR users.uuid <> activities.uuid_user_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: activities.user_id';
    END IF;

    -- activity type
    IF EXISTS(SELECT * FROM activities
      JOIN activity_types ON activity_types.id = activities.activity_type_id
    WHERE activities.id IS NOT NULL AND (activity_types.tenant_id <> activities.tenant_id OR activity_types.uuid <> activities.uuid_activity_type_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: activities.activity_type_id';
    END IF;

    -- activityable: leads
    IF EXISTS(SELECT * FROM activities
      JOIN leads cola ON cola.id = activities.activityable_id
    WHERE activities.id IS NOT NULL AND activities.activityable_type = 'leads' AND (cola.tenant_id <> activities.tenant_id OR cola.uuid <> activities.uuid_activityable_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: activities.activityable_id';
    END IF;

    -- activityable: accounts
    IF EXISTS(SELECT * FROM activities
      JOIN accounts cola ON cola.id = activities.activityable_id
    WHERE activities.id IS NOT NULL AND activities.activityable_type = 'accounts' AND (cola.tenant_id <> activities.tenant_id OR cola.uuid <> activities.uuid_activityable_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: activities.activityable_id';
    END IF;

    -- activityable: contacts
    IF EXISTS(SELECT * FROM activities
      JOIN contacts cola ON cola.id = activities.activityable_id
    WHERE activities.id IS NOT NULL AND activities.activityable_type = 'contacts' AND (cola.tenant_id <> activities.tenant_id OR cola.uuid <> activities.uuid_activityable_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: activities.activityable_id';
    END IF;

    -- activityable: opportunities
    IF EXISTS(SELECT * FROM activities
      JOIN opportunities cola ON cola.id = activities.activityable_id
    WHERE activities.id IS NOT NULL AND activities.activityable_type = 'opportunities' AND (cola.tenant_id <> activities.tenant_id OR cola.uuid <> activities.uuid_activityable_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: activities.activityable_id';
    END IF;




    -- CONTACTS -----------------------------------------------------------------------------
    -- source lead
    IF EXISTS(SELECT * FROM contacts
      JOIN leads ON leads.id = contacts.source_lead_id
    WHERE contacts.id IS NOT NULL AND (leads.tenant_id <> contacts.tenant_id OR leads.uuid <> contacts.uuid_source_lead_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: contacts.source_lead_id';
    END IF;

    -- csv import
    IF EXISTS(SELECT * FROM contacts
      JOIN csv_imports ON csv_imports.id = contacts.csv_import_id
    WHERE contacts.id IS NOT NULL AND (csv_imports.tenant_id <> contacts.tenant_id OR csv_imports.uuid <> contacts.uuid_csv_import_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: contacts.csv_import_id';
    END IF;

    -- user
    IF EXISTS(SELECT * FROM contacts
      JOIN users ON users.id = contacts.user_id
    WHERE contacts.id IS NOT NULL AND (users.tenant_id <> contacts.tenant_id OR users.uuid <> contacts.uuid_user_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: contacts.user_id';
    END IF;

    -- account
    IF EXISTS(SELECT * FROM contacts
      JOIN accounts ON accounts.id = contacts.account_id
    WHERE contacts.id IS NOT NULL AND (accounts.tenant_id <> contacts.tenant_id OR accounts.uuid <> contacts.uuid_account_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: contacts.account_id';
    END IF;




    -- CSV_IMPORTS --------------------------------------------------------------------------
    -- user
    IF EXISTS(SELECT * FROM csv_imports
      JOIN users ON users.id = csv_imports.user_id
    WHERE csv_imports.id IS NOT NULL AND (users.tenant_id <> csv_imports.tenant_id OR users.uuid <> csv_imports.uuid_user_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: csv_imports.user_id';
    END IF;




    -- DOCUMENTS ----------------------------------------------------------------------------
    -- user
    IF EXISTS(SELECT * FROM documents
      JOIN users ON users.id = documents.user_id
    WHERE documents.id IS NOT NULL AND (users.tenant_id <> documents.tenant_id OR users.uuid <> documents.uuid_user_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: documents.user_id';
    END IF;

    -- documentable: leads
    IF EXISTS(SELECT * FROM documents
      JOIN leads cola ON cola.id = documents.documentable_id
    WHERE documents.id IS NOT NULL AND documents.documentable_type = 'leads' AND (cola.tenant_id <> documents.tenant_id OR cola.uuid <> documents.uuid_documentable_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: documents.documentable_id';
    END IF;

    -- documentable: accounts
    IF EXISTS(SELECT * FROM documents
      JOIN accounts cola ON cola.id = documents.documentable_id
    WHERE documents.id IS NOT NULL AND documents.documentable_type = 'accounts' AND (cola.tenant_id <> documents.tenant_id OR cola.uuid <> documents.uuid_documentable_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: documents.documentable_id';
    END IF;

    -- documentable: contacts
    IF EXISTS(SELECT * FROM documents
      JOIN contacts cola ON cola.id = documents.documentable_id
    WHERE documents.id IS NOT NULL AND documents.documentable_type = 'contacts' AND (cola.tenant_id <> documents.tenant_id OR cola.uuid <> documents.uuid_documentable_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: documents.documentable_id';
    END IF;

    -- documentable: opportunities
    IF EXISTS(SELECT * FROM documents
      JOIN opportunities cola ON cola.id = documents.documentable_id
    WHERE documents.id IS NOT NULL AND documents.documentable_type = 'opportunities' AND (cola.tenant_id <> documents.tenant_id OR cola.uuid <> documents.uuid_documentable_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: documents.documentable_id';
    END IF;




    -- DYNAMICABLES -------------------------------------------------------------------------
    -- user
    IF EXISTS(SELECT * FROM dynamicables
      JOIN dynamic_fields ON dynamic_fields.id = dynamicables.dynamic_field_id
    WHERE dynamicables.id IS NOT NULL AND (dynamic_fields.tenant_id <> dynamicables.tenant_id OR dynamic_fields.uuid <> dynamicables.uuid_dynamic_field_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: dynamicables.dynamic_field_id';
    END IF;

    -- dynamicable: leads
    IF EXISTS(SELECT * FROM dynamicables
      JOIN leads cola ON cola.id = dynamicables.dynamicable_id
    WHERE dynamicables.id IS NOT NULL AND dynamicables.dynamicable_type = 'leads' AND (cola.tenant_id <> dynamicables.tenant_id OR cola.uuid <> dynamicables.uuid_dynamicable_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: dynamicables.dynamicable_id';
    END IF;

    -- dynamicable: accounts
    IF EXISTS(SELECT * FROM dynamicables
      JOIN accounts cola ON cola.id = dynamicables.dynamicable_id
    WHERE dynamicables.id IS NOT NULL AND dynamicables.dynamicable_type = 'accounts' AND (cola.tenant_id <> dynamicables.tenant_id OR cola.uuid <> dynamicables.uuid_dynamicable_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: dynamicables.dynamicable_id';
    END IF;

    -- dynamicable: contacts
    IF EXISTS(SELECT * FROM dynamicables
      JOIN contacts cola ON cola.id = dynamicables.dynamicable_id
    WHERE dynamicables.id IS NOT NULL AND dynamicables.dynamicable_type = 'contacts' AND (cola.tenant_id <> dynamicables.tenant_id OR cola.uuid <> dynamicables.uuid_dynamicable_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: dynamicables.dynamicable_id';
    END IF;

    -- dynamicable: opportunities
    IF EXISTS(SELECT * FROM dynamicables
      JOIN opportunities cola ON cola.id = dynamicables.dynamicable_id
    WHERE dynamicables.id IS NOT NULL AND dynamicables.dynamicable_type = 'opportunities' AND (cola.tenant_id <> dynamicables.tenant_id OR cola.uuid <> dynamicables.uuid_dynamicable_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: dynamicables.dynamicable_id';
    END IF;




    -- HISTORIES ----------------------------------------------------------------------------
    -- user
    IF EXISTS(SELECT * FROM histories
      JOIN users ON users.id = histories.user_id
    WHERE histories.id IS NOT NULL AND (users.tenant_id <> histories.tenant_id OR users.uuid <> histories.uuid_user_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: histories.user_id';
    END IF;

    -- historyable: leads
    IF EXISTS(SELECT * FROM histories
      JOIN leads cola ON cola.id = histories.historyable_id
    WHERE histories.id IS NOT NULL AND histories.historyable_type = 'leads' AND (cola.tenant_id <> histories.tenant_id OR cola.uuid <> histories.uuid_historyable_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: histories.historyable_id';
    END IF;

    -- historyable: accounts
    IF EXISTS(SELECT * FROM histories
      JOIN accounts cola ON cola.id = histories.historyable_id
    WHERE histories.id IS NOT NULL AND histories.historyable_type = 'accounts' AND (cola.tenant_id <> histories.tenant_id OR cola.uuid <> histories.uuid_historyable_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: histories.historyable_id';
    END IF;

    -- historyable: contacts
    IF EXISTS(SELECT * FROM histories
      JOIN contacts cola ON cola.id = histories.historyable_id
    WHERE histories.id IS NOT NULL AND histories.historyable_type = 'contacts' AND (cola.tenant_id <> histories.tenant_id OR cola.uuid <> histories.uuid_historyable_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: histories.historyable_id';
    END IF;

    -- historyable: opportunities
    IF EXISTS(SELECT * FROM histories
      JOIN opportunities cola ON cola.id = histories.historyable_id
    WHERE histories.id IS NOT NULL AND histories.historyable_type = 'opportunities' AND (cola.tenant_id <> histories.tenant_id OR cola.uuid <> histories.uuid_historyable_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: histories.historyable_id';
    END IF;




    -- LEADS --------------------------------------------------------------------------------
    -- csv import
    IF EXISTS(SELECT * FROM leads
      JOIN csv_imports ON csv_imports.id = leads.csv_import_id
    WHERE leads.id IS NOT NULL AND (csv_imports.tenant_id <> leads.tenant_id OR csv_imports.uuid <> leads.uuid_csv_import_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: leads.csv_import_id';
    END IF;

    -- user
    IF EXISTS(SELECT * FROM leads
      JOIN users ON users.id = leads.user_id
    WHERE leads.id IS NOT NULL AND (users.tenant_id <> leads.tenant_id OR users.uuid <> leads.uuid_user_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: leads.user_id';
    END IF;

    -- stage
    IF EXISTS(SELECT * FROM leads
      JOIN stages ON stages.id = leads.stage_id
    WHERE leads.id IS NOT NULL AND (stages.tenant_id <> leads.tenant_id OR stages.uuid <> leads.uuid_stage_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: leads.stage_id';
    END IF;




    -- LOST_REASONABLES ---------------------------------------------------------------------
    -- lost reason
    IF EXISTS(SELECT * FROM lost_reasonables
      JOIN lost_reasons ON lost_reasons.id = lost_reasonables.lost_reason_id
    WHERE lost_reasonables.id IS NOT NULL AND (lost_reasons.tenant_id <> lost_reasonables.tenant_id OR lost_reasons.uuid <> lost_reasonables.uuid_lost_reason_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: lost_reasonables.lost_reason_id';
    END IF;

    -- dynamicable: leads
    IF EXISTS(SELECT * FROM lost_reasonables
      JOIN leads cola ON cola.id = lost_reasonables.lost_reasonable_id
    WHERE lost_reasonables.id IS NOT NULL AND lost_reasonables.lost_reasonable_type = 'leads' AND (cola.tenant_id <> lost_reasonables.tenant_id OR cola.uuid <> lost_reasonables.uuid_lost_reasonable_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: lost_reasonables.lost_reasonable_id';
    END IF;

    -- dynamicable: accounts
    IF EXISTS(SELECT * FROM lost_reasonables
      JOIN accounts cola ON cola.id = lost_reasonables.lost_reasonable_id
    WHERE lost_reasonables.id IS NOT NULL AND lost_reasonables.lost_reasonable_type = 'accounts' AND (cola.tenant_id <> lost_reasonables.tenant_id OR cola.uuid <> lost_reasonables.uuid_lost_reasonable_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: lost_reasonables.lost_reasonable_id';
    END IF;

    -- dynamicable: contacts
    IF EXISTS(SELECT * FROM lost_reasonables
      JOIN contacts cola ON cola.id = lost_reasonables.lost_reasonable_id
    WHERE lost_reasonables.id IS NOT NULL AND lost_reasonables.lost_reasonable_type = 'contacts' AND (cola.tenant_id <> lost_reasonables.tenant_id OR cola.uuid <> lost_reasonables.uuid_lost_reasonable_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: lost_reasonables.lost_reasonable_id';
    END IF;

    -- dynamicable: opportunities
    IF EXISTS(SELECT * FROM lost_reasonables
      JOIN opportunities cola ON cola.id = lost_reasonables.lost_reasonable_id
    WHERE lost_reasonables.id IS NOT NULL AND lost_reasonables.lost_reasonable_type = 'opportunities' AND (cola.tenant_id <> lost_reasonables.tenant_id OR cola.uuid <> lost_reasonables.uuid_lost_reasonable_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: lost_reasonables.lost_reasonable_id';
    END IF;




    -- NOTES --------------------------------------------------------------------------------
    -- user
    IF EXISTS(SELECT * FROM notes
      JOIN users ON users.id = notes.user_id
    WHERE notes.id IS NOT NULL AND (users.tenant_id <> notes.tenant_id OR users.uuid <> notes.uuid_user_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: notes.user_id';
    END IF;

    -- historyable: leads
    IF EXISTS(SELECT * FROM notes
      JOIN leads cola ON cola.id = notes.noteable_id
    WHERE notes.id IS NOT NULL AND notes.noteable_type = 'leads' AND (cola.tenant_id <> notes.tenant_id OR cola.uuid <> notes.uuid_noteable_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: notes.noteable_id';
    END IF;

    -- historyable: accounts
    IF EXISTS(SELECT * FROM notes
      JOIN accounts cola ON cola.id = notes.noteable_id
    WHERE notes.id IS NOT NULL AND notes.noteable_type = 'accounts' AND (cola.tenant_id <> notes.tenant_id OR cola.uuid <> notes.uuid_noteable_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: notes.noteable_id';
    END IF;

    -- historyable: contacts
    IF EXISTS(SELECT * FROM notes
      JOIN contacts cola ON cola.id = notes.noteable_id
    WHERE notes.id IS NOT NULL AND notes.noteable_type = 'contacts' AND (cola.tenant_id <> notes.tenant_id OR cola.uuid <> notes.uuid_noteable_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: notes.noteable_id';
    END IF;

    -- historyable: opportunities
    IF EXISTS(SELECT * FROM notes
      JOIN opportunities cola ON cola.id = notes.noteable_id
    WHERE notes.id IS NOT NULL AND notes.noteable_type = 'opportunities' AND (cola.tenant_id <> notes.tenant_id OR cola.uuid <> notes.uuid_noteable_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: notes.noteable_id';
    END IF;




    -- OPPORTUNITIES ------------------------------------------------------------------------	
    -- user
    IF EXISTS(SELECT * FROM opportunities
      JOIN users ON users.id = opportunities.user_id
    WHERE opportunities.id IS NOT NULL AND (users.tenant_id <> opportunities.tenant_id OR users.uuid <> opportunities.uuid_user_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: opportunities.user_id';
    END IF;

    -- stage
    IF EXISTS(SELECT * FROM opportunities
      JOIN stages ON stages.id = opportunities.stage_id
    WHERE opportunities.id IS NOT NULL AND (stages.tenant_id <> opportunities.tenant_id OR stages.uuid <> opportunities.uuid_stage_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: opportunities.stage_id';
    END IF;

    -- account
    IF EXISTS(SELECT * FROM opportunities
      JOIN accounts ON accounts.id = opportunities.account_id
    WHERE opportunities.id IS NOT NULL AND (accounts.tenant_id <> opportunities.tenant_id OR accounts.uuid <> opportunities.uuid_account_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: opportunities.account_id';
    END IF;

    -- contact
    IF EXISTS(SELECT * FROM opportunities
      JOIN contacts ON contacts.id = opportunities.contact_id
    WHERE opportunities.id IS NOT NULL AND (contacts.tenant_id <> opportunities.tenant_id OR contacts.uuid <> opportunities.uuid_contact_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: opportunities.contact_id';
    END IF;




    -- ROLE_USER ----------------------------------------------------------------------------
    IF EXISTS(SELECT * FROM role_user
      JOIN roles ON role_user.role_id = roles.id
      JOIN users ON role_user.user_id = users.id
    WHERE roles.tenant_id <> users.tenant_id)
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: roles_user';
    END IF;




    -- STAGES -------------------------------------------------------------------------------
    -- ppeline
    IF EXISTS(SELECT * FROM stages
      JOIN pipelines ON pipelines.id = stages.pipeline_id
    WHERE stages.id IS NOT NULL AND (stages.tenant_id <> pipelines.tenant_id OR pipelines.uuid <> stages.uuid_pipeline_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: stages.pipeline_id';
    END IF;




    -- USERS --------------------------------------------------------------------------------
    -- user
    IF EXISTS(SELECT * FROM users u1
      JOIN users u2 ON u1.id = u2.parent_id
    WHERE u1.id IS NOT NULL AND u1.parent_id IS NOT NULL AND (u1.tenant_id <> u2.tenant_id OR u1.uuid <> u2.uuid_parent_id))
    THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR: users.parent_id';
    END IF;

  END $$

DELIMITER ;

CALL do_test();
 