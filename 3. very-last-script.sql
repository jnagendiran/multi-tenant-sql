DELIMITER |

DROP TRIGGER accounts_before_insert;
DROP TRIGGER activities_before_insert;
DROP TRIGGER activity_types_before_insert;
DROP TRIGGER contacts_before_insert;
DROP TRIGGER csv_imports_before_insert;
DROP TRIGGER documents_before_insert;
DROP TRIGGER dynamic_fields_before_insert;
DROP TRIGGER dynamicables_before_insert;
DROP TRIGGER histories_before_insert;
DROP TRIGGER keymaps_before_insert;
DROP TRIGGER leads_before_insert;
DROP TRIGGER lost_reasonables_before_insert;
DROP TRIGGER lost_reasons_before_insert;
DROP TRIGGER notes_before_insert;
DROP TRIGGER opportunities_before_insert;
DROP TRIGGER permissions_before_insert;
DROP TRIGGER pipelines_before_insert;
DROP TRIGGER roles_before_insert;
DROP TRIGGER settings_before_insert;
DROP TRIGGER stages_before_insert;
DROP TRIGGER users_before_insert;

# tenant_id = accounts, activities, activity_types, contacts, csv_imports, documents, dynamic_fields, dynamicables, histories, keymaps, leads, lost_reasonables, lost_reasons, notes, opportunities,
# pipelines, roles, settings, stages, users

# CREATE TRIGGER accounts_before_insert
# BEFORE INSERT ON accounts
# FOR EACH ROW
#   BEGIN
#     SET NEW.id = UUID();
#   END;
# |
# CREATE TRIGGER activities_before_insert
# BEFORE INSERT ON activities
# FOR EACH ROW
#   BEGIN
#     SET NEW.id = UUID();
#   END;
# |
# CREATE TRIGGER activity_types_before_insert
# BEFORE INSERT ON activity_types
# FOR EACH ROW
#   BEGIN
#     SET NEW.id = UUID();
#   END;
# |
# CREATE TRIGGER contacts_before_insert
# BEFORE INSERT ON contacts
# FOR EACH ROW
#   BEGIN
#     SET NEW.id = UUID();
#   END;
# |
# CREATE TRIGGER csv_imports_before_insert
# BEFORE INSERT ON csv_imports
# FOR EACH ROW
#   BEGIN
#     SET NEW.id = UUID();
#   END;
# |
# CREATE TRIGGER documents_before_insert
# BEFORE INSERT ON documents
# FOR EACH ROW
#   BEGIN
#     SET NEW.id = UUID();
#   END;
# |
# CREATE TRIGGER dynamic_fields_before_insert
# BEFORE INSERT ON dynamic_fields
# FOR EACH ROW
#   BEGIN
#     SET NEW.id = UUID();
#   END;
# |
# CREATE TRIGGER dynamicables_before_insert
# BEFORE INSERT ON dynamicables
# FOR EACH ROW
#   BEGIN
#     SET NEW.id = UUID();
#   END;
# |
# CREATE TRIGGER histories_before_insert
# BEFORE INSERT ON histories
# FOR EACH ROW
#   BEGIN
#     SET NEW.id = UUID();
#   END;
# |
# CREATE TRIGGER keymaps_before_insert
# BEFORE INSERT ON keymaps
# FOR EACH ROW
#   BEGIN
#     SET NEW.id = UUID();
#   END;
# |
# CREATE TRIGGER leads_before_insert
# BEFORE INSERT ON leads
# FOR EACH ROW
#   BEGIN
#     SET NEW.id = UUID();
#   END;
# |
# CREATE TRIGGER lost_reasonables_before_insert
# BEFORE INSERT ON lost_reasonables
# FOR EACH ROW
#   BEGIN
#     SET NEW.id = UUID();
#   END;
# |
# CREATE TRIGGER lost_reasons_before_insert
# BEFORE INSERT ON lost_reasons
# FOR EACH ROW
#   BEGIN
#     SET NEW.id = UUID();
#   END;
# |
# CREATE TRIGGER notes_before_insert
# BEFORE INSERT ON notes
# FOR EACH ROW
#   BEGIN
#     SET NEW.id = UUID();
#   END;
# |
# CREATE TRIGGER opportunities_before_insert
# BEFORE INSERT ON opportunities
# FOR EACH ROW
#   BEGIN
#     SET NEW.id = UUID();
#   END;
# |
# CREATE TRIGGER permissions_before_insert
# BEFORE INSERT ON permissions
# FOR EACH ROW
#   BEGIN
#     SET NEW.id = UUID();
#   END;
# |
# CREATE TRIGGER pipelines_before_insert
# BEFORE INSERT ON pipelines
# FOR EACH ROW
#   BEGIN
#     SET NEW.id = UUID();
#   END;
# |
# CREATE TRIGGER roles_before_insert
# BEFORE INSERT ON roles
# FOR EACH ROW
#   BEGIN
#     SET NEW.id = UUID();
#   END;
# |
# CREATE TRIGGER settings_before_insert
# BEFORE INSERT ON settings
# FOR EACH ROW
#   BEGIN
#     SET NEW.id = UUID();
#   END;
# |
# CREATE TRIGGER stages_before_insert
# BEFORE INSERT ON stages
# FOR EACH ROW
#   BEGIN
#     SET NEW.id = UUID();
#   END;
# |
# CREATE TRIGGER users_before_insert
# BEFORE INSERT ON users
# FOR EACH ROW
#   BEGIN
#     SET NEW.id = UUID();
#   END;
# |
# DELIMITER ;